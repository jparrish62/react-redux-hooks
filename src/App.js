import React from 'react';
import './App.css'
import Workouts from './components/workouts'

function App() {
  return (
    <div className="App">
			<h1>Work Tracker</h1>
				<Workouts />
    </div>
  );
}

export default App;
