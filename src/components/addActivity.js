import React, { useState } from 'react'
import { useDispatch } from 'react-redux'

const AddActivity = () => {
	const [data, setData] = useState({})
	const dispatch = useDispatch(); 

	const addActivity = () => {
		dispatch({
			type: "CREAT_ACTIVITY",
			payload: {
				name: data.name,
				duration: data.duration
			}
		})
	}
	const handleChange = (e) => {
		e.persist();
		setData(prev => ({...prev, [e.target.name]: e.target.value}))
	}
	return (
		<div> 
			<div>
				<p>Activity:</p>
					<input onChange={(e) => handleChange(e)} name={"name"} placeholder={"Activity Name..."}/>
			</div>

			<div>
				<p>Duration:</p>
					<input onChange={(e) => handleChange(e)} name={"duration"} placeholder={"Duration"}/>
			</div>
				<button onClick={addActivity}>Add Activity</button>
		</div>
	)
}

export default AddActivity
